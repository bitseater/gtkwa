/*
* Copyright (c) Carlos Suárez (https://gitlab.com/bitseater)
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; If not, see <http://www.gnu.org/licenses/>.
*
* Authored by: Carlos Suárez <bitseater@gmail.com>
*/
namespace gtkWA {

    public class MainWindow : Gtk.Window {

        public gtkWAApp app;
        private Settings settings;

        public MainWindow (gtkWAApp app) {
            this.app = app;
            this.application = app;

            //Geometry and theme
            settings = new Settings ("com.gitlab.bitseater.gtkwa");
            this.set_default_size(settings.get_int ("width"), settings.get_int ("height"));
            notify["default-width"].connect(() => {
                int width, height;
                this.get_default_size (out width, out height);
                settings.set_int ("width", width);
                settings.set_int ("height", height);
            });

            //Create headerbar
            var header = new Gtk.HeaderBar ();
            header.title_widget = new Gtk.Label ("gtkWA");
            header.show_title_buttons = true;

            var on_quit = new GLib.SimpleAction ("on_quit", null);
            on_quit.activate.connect (() => {
                app.quit ();
            });
            app.add_action (on_quit);
            var menu = new GLib.Menu ();
            var section = new GLib.Menu ();
            var quit_item = new GLib.MenuItem (_("Quit gtkWA"), "app.on_quit");
            section.append_item (quit_item);
            menu.append_section (null, section);
			var appbutton = new Gtk.MenuButton ();
			appbutton.set_icon_name ("open-menu-symbolic");
			appbutton.menu_model = menu;
            header.pack_end (appbutton);

            //Compose mainwindow
            var wa_view = new WebKit.WebView ();
            wa_view.halign = Gtk.Align.FILL;
            wa_view.valign = Gtk.Align.FILL;
            wa_view.load_uri ("https://web.whatsapp.com/");

            set_titlebar (header);
            child=wa_view;
            present ();
        }
    }
}
