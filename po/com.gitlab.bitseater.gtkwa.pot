# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-04-20 11:20+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/com.gitlab.bitseater.gtkwa.appdata.xml.in:8
msgid "A Gtk frontend for Whatsapp Web"
msgstr ""

#: data/com.gitlab.bitseater.gtkwa.appdata.xml.in:26
msgid "Carlos Su\303\241rez"
msgstr ""

#: data/com.gitlab.bitseater.gtkwa.appdata.xml.in:13
msgid "Events."
msgstr ""

#: data/com.gitlab.bitseater.gtkwa.appdata.xml.in:32
msgid "Initial release"
msgstr ""

#: data/com.gitlab.bitseater.gtkwa.appdata.xml.in:14
msgid "Issues."
msgstr ""

#: data/com.gitlab.bitseater.gtkwa.appdata.xml.in:12
msgid "News."
msgstr ""

#: src/MainWindow.vala:52
msgid "Quit gtkWA"
msgstr ""

#: data/com.gitlab.bitseater.gtkwa.desktop.in:5
msgid "WA Web in a gtk-window"
msgstr ""

#: data/com.gitlab.bitseater.gtkwa.appdata.xml.in:10
msgid "Whatsapp Web on a gtk window built with Vala and Gtk."
msgstr ""

#: data/com.gitlab.bitseater.gtkwa.desktop.in:13
msgid "Whatsapp;"
msgstr ""

#: data/com.gitlab.bitseater.gtkwa.appdata.xml.in:15
msgid "etc..."
msgstr ""

#: data/com.gitlab.bitseater.gtkwa.appdata.xml.in:7
#: data/com.gitlab.bitseater.gtkwa.desktop.in:3
#: data/com.gitlab.bitseater.gtkwa.desktop.in:4
msgid "gtkWA"
msgstr ""
